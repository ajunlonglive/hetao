cd %~dp0
%~d0

mkdir "\Program Files"

mkdir "\Program Files\hetao"

mkdir "\Program Files\hetao\bin"
copy "win\bin\*" "\Program Files\hetao\bin" /Y

mkdir "\Program Files\hetao\lib"
copy "win\lib\*" "\Program Files\hetao\lib" /Y

mkdir "\Program Files\hetao\include"
copy "win\include\*" "\Program Files\hetao\include" /Y
mkdir "\Program Files\hetao\include\openssl"
copy "win\include\openssl\*" "\Program Files\hetao\include\openssl\" /Y

mkdir "\Program Files\hetao\conf"
if exist "\Program Files\hetao\conf\hetao.conf" goto skip_copy_hetao_conf
copy "conf\hetao-WINDOWS.conf" "\Program Files\hetao\conf\hetao.conf" /Y
:skip_copy_hetao_conf
if exist "\Program Files\hetao\conf\hetao.conf.VC2008DEBUG" goto skip_copy_hetao_VC2008DEBUG_conf
copy "conf\hetao-VC2008DEBUG.conf" "\Program Files\hetao\conf\hetao-VC2008DEBUG.conf" /Y
:skip_copy_hetao_VC2008DEBUG_conf

mkdir "\Program Files\hetao\www"
if exist "\Program Files\hetao\www\index.html" goto skip_copy_index_html
copy "www\index.html" "\Program Files\hetao\www\index.html" /Y
:skip_copy_index_html
if exist "\Program Files\hetao\www\mydir\index.html" goto skip_copy_mydir_index_html
mkdir "\Program Files\hetao\www\mydir"
copy "www\mydir\index.html" "\Program Files\hetao\www\mydir\index.html" /Y
:skip_copy_mydir_index_html
if exist "\Program Files\hetao\www\mydir\mydir2\index.html" goto skip_copy_mydir_mydir2_index_html
mkdir "\Program Files\hetao\www\mydir\mydir2"
copy "www\mydir\mydir2\index.html" "\Program Files\hetao\www\mydir\mydir2\index.html" /Y
:skip_copy_mydir_mydir2_index_html
if exist "\Program Files\hetao\www\error_pages" goto skip_copy_error_pages
mkdir "\Program Files\hetao\www\error_pages"
xcopy "www\error_pages" "\Program Files\hetao\www\error_pages"
:skip_copy_error_pages

mkdir "\Program Files\hetao\log"

pause
