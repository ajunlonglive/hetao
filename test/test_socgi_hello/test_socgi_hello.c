#include "hetao_socgi.h"

#include "LOGC.h"

INITHTTPAPPLICATION InitHttpApplication ;
int InitHttpApplication( struct HttpApplicationContext *ctx )
{
	InfoLog( __FILE__ , __LINE__ , "InitHttpApplication" );
	
	return HTTP_OK;
}

CALLHTTPAPPLICATION CallHttpApplication ;
int CallHttpApplication( struct HttpApplicationContext *ctx )
{
	char	http_body[ 1024 ] ;
	int	http_body_len ;
	
	int	nret = 0 ;
	
	InfoLog( __FILE__ , __LINE__ , "CallHttpApplication" );
	
	memset( http_body , 0x00 , sizeof(http_body) );
	http_body_len = SNPRINTF( http_body , sizeof(http_body)-1 , "hello test_socgi_hello.socgi , my config filename is [%s][%s]\n" , SOCGIGetCurrentPathname(ctx) , SOCGIGetConfigPathfilename(ctx) ) ;
	nret = SOCGIFormatHttpResponse( ctx , http_body , http_body_len , NULL ) ;
	if( nret )
	{
		ErrorLog( __FILE__ , __LINE__ , "SOCGIFormatHttpResponse failed[%d]" , nret );
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	else
	{
		InfoLog( __FILE__ , __LINE__ , "SOCGIFormatHttpResponse ok" );
		return HTTP_OK;
	}
}

CLEANHTTPAPPLICATION CleanHttpApplication ;
int CleanHttpApplication( struct HttpApplicationContext *ctx )
{
	InfoLog( __FILE__ , __LINE__ , "CleanHttpApplication" );
	
	return HTTP_OK;
}

